import xbmcaddon, xbmcgui, xbmc
from threading import Thread
import time
import vobject
import dbus
import random
import dbus.service
import datetime
import os
import re
import textwrap

import subprocess

__addon__     = xbmcaddon.Addon()
__addonpath__ = __addon__.getAddonInfo('path').decode("utf-8")
__hf_addon__       = xbmcaddon.Addon(id='script.service.handsfree')
__hf_addondir__    = xbmc.translatePath( __hf_addon__.getAddonInfo('profile') )



#capture a couple of actions to close the window
ACTION_PREVIOUS_MENU = 10
ACTION_BACK = 92

#controls for mini GUI
TITLE_LABEL  = 1402
TXT_LABEL    = 1403

#controls for full GUI
BACK_BTN     = 1401

TXT_POP      = 1420
PERSON_IMG   = 1421
OK_BTN       = 1422
CHECK_BTN    = 1423
GRN_CHECK    = 1424
POPUP_TIME   = 5
MSG_TIME     = 7

__counter__  = POPUP_TIME

# Logging facility.  For some reason this randomly throws errors in the CallRemoved signal
def log(logline):
    try:
        print "TXTXML: " + logline
    except:
        pass

def set_kodi_prop(property, value):
#    log ("Setting: '{}', '{}'".format(property, value))
    strvalue=str(value)
    xbmcgui.Window(10000).setProperty(property, strvalue)

def get_kodi_prop(property):
    value=xbmcgui.Window(10000).getProperty(property)
    log("Loading: '" + property + "', '" + value + "'")    
    
    return value

def updateWindow():
    global __counter__
    #this is the worker thread that updates the window information every w seconds
    #this strange looping exists because I didn't want to sleep the thread for very long
    #as time.sleep() keeps user input from being acted upon

   
    while __counter__ > 0:
        
        try:
            # sometime this throws an exception. Probably because read_btn isn't quite initialized
            txtnotify.read_btn.setLabel("OK (" + "{:.0f}".format(__counter__) + ")")
            __counter__=__counter__-.25
        except:
            pass
        time.sleep(.25)
    txt_dialog.leave()        
       

class txtnotify(xbmcgui.WindowXMLDialog):
    
    title_label=None
    txt_pop = None
    title=""

    read_btn=None
    ignore_btn=None
    person_img=None
    grn_check=None
    
    global __counter__
    
    def __init__(self,strXMLname, strFallbackPath, strDefaultName, forceFallback):
        log("Initializing...")

    def leave(self):
        log("ENDING..")
        self.close()
        
    def onInit(self):

        txtnotify.txt_label=self.getControl(TXT_LABEL)
        
        txt=get_kodi_prop("txt_data")
        txt_w=textwrap.wrap(txt,45)
        new_txt=""
        
        for t in txt_w:
            new_txt=new_txt+t+"[CR]"

        if len(txt)>225:
            new_txt=new_txt[:225]+"..."

        set_kodi_prop("txt_data", new_txt)
        
        txtnotify.txt_pop=self.getControl(TXT_POP)
        txtnotify.read_btn=self.getControl(OK_BTN)
        txtnotify.ignore_btn=self.getControl(CHECK_BTN)
        txtnotify.person_img=self.getControl(PERSON_IMG)
        txtnotify.grn_check=self.getControl(GRN_CHECK)
        self.grn_check.setVisible(False)
        # This is loaded last, because we use it in the main looping thread
        # to make sure this function has completed initialization
        txtnotify.title_label=self.getControl(TITLE_LABEL)

        image_str=self.load_photo(get_kodi_prop("txt_from"))
        txtnotify.person_img.setImage(image_str)

    def onAction(self, action):
        if action == ACTION_PREVIOUS_MENU:
            log("ENDING. prev menu")
            __counter__=0

    def onClick(self, controlID):
        global __counter__        
        log("On click: " + str(controlID))
        
        if controlID == BACK_BTN:
            log("ENDING. back button")
            __counter__=0

        if controlID == OK_BTN:
            log("Counter END: " + str(__counter__))
            __counter__=0

        if controlID == CHECK_BTN:
            self.grn_check.setVisible(True)
            muted=get_kodi_prop("muted_cnt")
            if muted=="":
                muted="1"
            else:
                muted=str(int(muted)+1)
            set_kodi_prop("muted_cnt", muted)
            set_kodi_prop("muted_sender"+muted, get_kodi_prop("txt_from"))
            
        
    def onFocus(self, controlID):
        pass
    
    def onControl(self, controlID):
        pass

    def load_photo(self, name):
        log("Loading image..")
        image_str=__hf_addondir__ + name + ".jpg"
        # If there isn't an image for this caller load a silhouette
        if os.path.isfile(image_str)==False:
            silhouette=random.randint(1, 4)
            name="silhouette" + str(silhouette)
            image_str=name + ".png"
        log(image_str)
        return image_str
        
if  __name__ == '__main__':    
    
    t1 = Thread( target=updateWindow)
    t1.setDaemon( True )
    t1.start()
 
    txt_dialog = txtnotify("txtnotify.xml", __addonpath__, 'default', '720p')
    txt_dialog.doModal()
    del txt_dialog
 